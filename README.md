# Composite coupon testing

## Coupon testing - Round 1
<a href="http://fab.cba.mit.edu/classes/863.16/doc/tutorials/composites/coupon-testing.html">Link to jupyter notebook</a>
  

## Coupon testing - Round 2
<a href="coupon_test_round_2.html">Link to jupyter notebook</a>

In this round of testing, Ryan, Tom and Sam tried out a range of fabrics using the same layup technique.

This is a <a href="https://docs.google.com/spreadsheets/d/1HgWXHEF67vbYZ3gqf5KXB3R6DSsONPY9Ah2PjuKFmlA/edit#gid=0">Google doc where</a> the materials and their sources are listed</p>

The Jupyter notebook links are static copies taken using <code>jupyter nbconvert file_name.ipynb</code> so that they can be viewed easily

## Alternative fabrics
### Bamboo
Some research has been conducted into Bamboo Reinforced Fiber Polymer (BRFP) materials. <a href="http://www.mse.umd.edu/sites/default/files/documents/undergrad/enma490/Team%20Bamboo%20Fiber%20Composite-Report.pdf">This report</a> from the Universtiy of Maryland provides some good detail on the state of the art of BRFP's. <a href="http://www.swicofil.com/products/015bamboo.html">Swicofil</a> are mentioned in a couple of places on the internet as producers of bamboo fibre. A project was carried out between Cambridge University, MIT and UBC around Structural Bamboo products. The focus in this project was understanding the mechanical properties of a bamboo laminate composite material as an alternative to wood based construction materials. <a href="http://www.sciencedirect.com/science/article/pii/S0950061815001956">This paper details some of the testing that they carried out.</a>

The University of Maryland team provide a nice description of the technique that they used to process the bamboo to create a weave:

<i>In order to prototype our composite we needed to make a weave from bamboo fibers and incorporate them into epoxy. We started with bamboo which we harvested from a local garden. The dimensions were roughly 2 inches in diameter and 25 feet long. The 25 foot long sections were cut on either sides of the nodes then cut longitudinally splitting the bamboo culm into six sections. Once sectioned and separated, the bamboo was soaked in a 0.1 molar solution of sodium hydroxide to delignifying the bamboo. Our previous research [3] suggested that this is important in order to promote adhesion between the bamboo and epoxy. After the bamboo soaked for 72 hours, it was soaked in water for 3 hours and rinsed several times to remove any remaining sodium hydroxide. After being thoroughly rinsed the bamboo was placed in an oven at 120 C for 2 hours then allowed to air dry for five days. A roller mill was used next to break down the bamboo sections into splintered bamboo. The splinter bamboo facilitated the separation of the bamboo fibers. The splintered bamboo was then soaked in water again to increase the flexibility of the fibers during final separation. Without wetting the splintered bamboo, the fibers were difficult to extract without breaking them. Once the fibers were extracted, they were collected into bundles of eight fibers. The bundles were then mounted into a loom, from which a weave was constructed. A simple plain weave pattern was then used which consists of an alternating over-under pattern. The weave was then inserted into the mold.</i>

A company called Swicofil include <a href="http://www.swicofil.com/bamboo.pdf">bamboo fibers on their website</a>. However, their PDF indicates that the material properties of the fabric that they produce does not retain the mechanical properties of the bamboo fibers. It seems that they are producing something closer to a viscose rayon fabric.

A group from Columbia has done some good work in the <a href="http://www.sciencedirect.com/science/article/pii/S1359836812002788">development and characterization of a sustainable composite</a> using polylactic acid (PLA) and a woven bamboo fabric.